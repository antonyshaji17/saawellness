
<section class="video-one jarallax pt-5" data-jarallax="" data-speed="0.3" data-imgposition="50% 50%">
    <div class="container text-center">
        <h1>Newsletter</h1>
        <p>Get insider access for limited edition offers, new launches, insights on Ayurvedic, skincare, beauty and healthy tips and more!</p>
        <div class="form-check" >
  <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" style=" margin-top: 0.7rem; " checked>
  <label class="form-check-label" for="flexCheckChecked">
  I have read and agree to Saa wellness Terms of Conditions  </label>
</div>
        <div class="row justify-content-center align-items-center mt-2">
            <div class="col-md-12 " style="left: 25%;">
            <form>
            <div class="form-inline">
            <div class="form-group mr-2">
                <input type="text" class="form-control" placeholder="Name"/>
            </div>
            <div class="form-group mr-2">
                <input type="text" class="form-control" placeholder="Email"/>
            </div>
            <button type="submit" class="btn btn-primary golden">SUBSCRIBE</button>

        </div>
</form>
            </div>
        </div>
    </div><!-- /.container -->
    <div id="jarallax-container-0" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; z-index: -100;"><img src="assets/images/backgrounds/newsletter.png" alt="" class="jarallax-img" style="object-fit: cover; object-position: 50% 50%; max-width: none; position: fixed; top: 0px; left: 0px; width: 1464.8px; height: 446px; overflow: hidden; pointer-events: none; transform-style: preserve-3d; backface-visibility: hidden; will-change: transform, opacity; margin-top: -30px; transform: translate3d(0px, 71.625px, 0px);"></div>
</section>
