<style>
    .category {
        position: relative;
        width: 50%;
    }

    .image {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
    }

    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
    }

    .category:hover .image {
        opacity: 0.3;
        border: 15px solid black;
    }

    .category:hover .middle {
        opacity: 1;
    }

    .text {
        background-color: none;
        color: black;
        font-size: 22px;
        padding: 16px 32px;
        font-weight:700;
    }
</style>
<div class="row mx-5 mt-5">
    <div class="col-md-12 section-title">
        <h3>OUR CATEGORIES</h3>
    </div>
    <div class="row">
        <div class="col-md-3 pl-2 category">
            <img src="./assets/images/category/honey.jpg" alt="Avatar" class="image" style="width:100%">
            <div class="middle">
                <div class="text">CLEANSE</div>
            </div>
        </div>

        <div class="col-md-3 pl-2 category">
            <img src="./assets/images/category/honey.jpg" alt="Avatar" class="image" style="width:100%">
            <div class="middle">
                <div class="text">CLEANSE</div>
            </div>
        </div>
        <div class="col-md-3 pl-2 category">
            <img src="./assets/images/category/honey.jpg" alt="Avatar" class="image" style="width:100%">
            <div class="middle">
                <div class="text">CLEANSE</div>
            </div>
        </div>
        <div class="col-md-3 pl-2 category">
            <img src="./assets/images/category/honey.jpg" alt="Avatar" class="image" style="width:100%">
            <div class="middle">
                <div class="text">CLEANSE</div>
            </div>
        </div>

    </div>
    <div class="row mt-3">
        <div class="col-md-3 pl-2 category">
            <img src="./assets/images/category/honey.jpg" alt="Avatar" class="image" style="width:100%">
            <div class="middle">
                <div class="text">CLEANSE</div>
            </div>
        </div>
        <div class="col-md-3 pl-2 category">
            <img src="./assets/images/category/honey.jpg" alt="Avatar" class="image" style="width:100%">
            <div class="middle">
                <div class="text">CLEANSE</div>
            </div>
        </div>
        <div class="col-md-3 pl-2 category">
            <img src="./assets/images/category/honey.jpg" alt="Avatar" class="image" style="width:100%">
            <div class="middle">
                <div class="text">CLEANSE</div>
            </div>
        </div>
        <div class="col-md-3 pl-2 category">
            <img src="./assets/images/category/honey.jpg" alt="Avatar" class="image" style="width:100%">
            <div class="middle">
                <div class="text">CLEANSE</div>
            </div>
        </div>
    </div>
</div>