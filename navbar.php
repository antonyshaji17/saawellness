<div class="preloader">
        <img class="preloader__image" width="55" src="assets/images/saalogo.png" alt="" />
    </div>
    <!-- /.preloader -->
    <div class="page-wrapper">
        <header class="main-header">
            <nav class="main-menu">
                <div class="container">
                    <ul class="main-menu__list">
                        <li class="dropdown">
                            <a href="index.php">HOME</a>
                        </li>
                        <li>
                            <a href="#products">PRODUCTS</a>
                        </li>
                        <li class="dropdown">
                        <a href="index.php" class="logo">
                            <img src="assets/images/saalogo.png" width="105" alt="">
                        </a>
                        </li>
                        <li class="dropdown"><a href="#about">ABOUT</a>
                        </li>
                        <li><a href="#contact">CONTACT</a></li>
                    </ul>
                </div><!-- /.container -->
            </nav>
            <!-- /.main-menu -->
        </header><!-- /.main-header -->

        <div class="stricky-header stricked-menu main-menu">
            <div class="sticky-header__content"></div><!-- /.sticky-header__content -->
        </div><!-- /.stricky-header -->
        <div class="col-xs-12" style="height:20px;"></div>
