<div class="row">
    <div class="col-md-6 product-image" style="background-image: url('./assets/images/backgrounds/plate.jpg'); height: 70vh">

    </div>
    <div class="col-md-6 align-self-center text-center" style="padding-right: 5%;">
        <div class="row">
            <div class="col-md-12" style="line-height: 1px;">
                <h1>Wellness | Ritual</h1>
                <p>Wellness | Ritual</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>Saa is for the Organic Self Care Naturalist. Break free of indoctrination. We broke free of all jargon . Saa Ritual is simple , personable and totally focussed on the intuitive . Six blends that add fragrance and flavour to self care. Pure wellness on first principles .The Origins are precious, authentic and blended to perfection to be understated. Aromatherapy and Naturopathy anchored by a tempered Siddha practice. Prompted by impulses that elicit a response, elevate the experience of self care with high quality true origin, ethically sourced, small batch product free of chemicals and usable at any time anywhere in the world . Easy, uncomplicated, stress free, regimen free.</p>
            </div>
        </div>

    </div>
</div>
<div class="row mb-5">
    <div class="col-md-6 col-lg-6 " style="padding-right: 5%;">
        <div class="row pl-3 pt-3">
            <div class="col-md-12" style="line-height: 1px;">
                <h3>FIVE ELEMENTS OF SELF CARE</h3>
            </div>
        </div>

        <div class="row p-3">
            <div class="col-md-12 col-lg-12">
                <div class="about-one__box">
                    <span>
                        <h3><i class="fa fa-check-circle"></i> NOURISH</h3>- Spiced Oils for hair, face, body,for application ,massage, packs and soak.
                    </span>
                </div>
            </div>
            <div class="col-md-12 col-lg-12">
                <div class="about-one__box">
                    <span>
                        <h3><i class="fa fa-check-circle"></i> RENEW</h3> - Organic wellness in the form of a Scrub , Pack , Oil and Poultice.
                        <br> <span>NOURISH and RENEW can be used as seasoning or cooking to enhance efficacy of Immunity Boost.</span>
                    </span>
                </div>
            </div>
            <div class="col-md-12 col-lg-12">
                <div class="about-one__box">
                    <h3><i class="fa fa-check-circle"></i> CLEANSE</h3>
                    - Purest composition of wash for hair, face, body and hands.
                </div>
            </div>
            <div class="col-md-12 col-lg-12">
                <div class="about-one__box">
                    <h3><i class="fa fa-check-circle"></i> SOOTHE</h3>
                    - Rarest of virgin Aloe Gel with pure essential oil for face,body, hands, feet, lips.
                </div>
            </div>
            <div class="col-md-12 col-lg-12">
                <div class="about-one__box">
                    <h3><i class="fa fa-check-circle"></i> REVIVE</h3>
                    <p> - Moisturiser for body, face , hands , feet
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6" style="background-image: url('./assets/images/backgrounds/plate.jpg'); height: 70vh">
    </div>
</div>